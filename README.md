# SIMRS Dunda Webservices #

Webservice System Management Rumah Sakit dr.M.M Dunda Limboto.

##### Table Of Contents
[1. Pasien](#pasien) <br>
  * [Get All Pasien](#get-all-pasien)<br>
  * [Get Pasein By NOMR](#get-pasien-by-nomr)<br>

[2. Kamar](#kamar) <br>
  * [Get All Kamar](#get-all-kamar)<br>
  * [Get Kamar By KODE](#get-kamar-by-kode)<br>


[2. BED](#bed) <br>
  * [Get All Bed](#get-all-bed)<br>
  * [Get Bed By Id](#get-bed-by-id)<br>


<a name="pasien"/>

### 1. Pasien

<a name="get-all-pasien" />

* GET All Data Pasien
    * URL ```http://BASE_URL/api/v1/pasien```
    * Method ```GET```
```json
{
    "code": 200,
    "status": "Success",
    "data": [
        {
            "id": 1,
            "tgllahir": "2008-10-19",
            "nomr": "000000",
            "title": "AN",
            "nama": "MOH IRSAD MANGOPA",
            "tempat": "GORONTALO",
            ...
        }]
}
```

<a name="get-pasien-by-nomr" />

* GET Pasien By NOMR
  * URL ```http://BASE_URL/api/v1/pasien/{NOMR}```
  * Method ```GET```

```json
{
    "code": 200,
    "status": "Success",
    "data": {
        "id": 1,
        "tgllahir": "2008-10-19",
        "nomr": "000000",
        "title": "AN",
        "nama": "MOH IRSAD MANGOPA",
        "tempat": "GORONTALO",
        ...
    }
}
```



<a name="kamar" />

### 1. Kamar

<a name="get-all-kamar" />

* GET All Kamar
  * URL ```http://BASE_URL/api/v1/kamar```
  * Method ```GET```
```json
{
  "code": 200,
  "status": "Success",
  "data": [{
    "id": 1,
    "kode": "KIRH-01",
    "namaKamar": "Kamar Irina H 1",
    "ruangan": {
      "id": 1,
      "ruangan": "Irina H Lantai 1",
      "unitLayanan": {
        "id": 2,
        "namaUnitLayanan": "Perawatan Penyakit Dalam",
        "status": "Y"
      },
      "status": "Y"
    },
    "kelas": {
      "id": 3,
      "kelas": "Kelas II",
      "tarif_inap": 75000,
      "idperbub": null,
      "status": "Y"
    },
    "status": "Y"
  }],
  ...
}
```



<a name="get-kamar-by-kode" />

* GET Kamar By KODE
  * URL ```http://BASE_URL/api/v1/kamar/{kode}```
  * Method ```GET```
```json
{
  "code": 200,
  "status": "Success",
  "data": {
    "id": 1,
    "kode": "KIRH-01",
    "namaKamar": "Kamar Irina H 1",
    "ruangan": {
      "id": 1,
      "ruangan": "Irina H Lantai 1",
      "unitLayanan": {
        "id": 2,
        "namaUnitLayanan": "Perawatan Penyakit Dalam",
        "status": "Y"
      },
      "status": "Y"
    },
    "kelas": {
      "id": 3,
      "kelas": "Kelas II",
      "tarif_inap": 75000,
      "idperbub": null,
      "status": "Y"
    },
    "status": "Y"
  }
}
```



<a name="bed" />

### 1. Bed

<a name="get-all-bed" />

* GET All Bed
  * URL ```http://BASE_URL/api/v1/bed```
  * Method ```GET```
```json
{
  "code": 200,
  "status": "Success",
  "data": [{
    "id": 1,
    "kodeBed": "B1",
    "noBed": "1",
    "kamar": {
      "id": 1,
      "kode": "KIRH-01",
      "namaKamar": "Kamar Irina H 1",
      "ruangan": {
        "id": 1,
        "ruangan": "Irina H Lantai 1",
        "unitLayanan": {
          "id": 2,
          "namaUnitLayanan": "Perawatan Penyakit Dalam",
          "status": "Y"
        },
        "status": "Y"
      },
      "kelas": {
        "id": 3,
        "kelas": "Kelas II",
        "tarif_inap": 75000,
        "idperbub": null,
        "status": "Y"
      },
      "status": "Y"
    },
    "status": "Terisi"
  }],
  ...
}
```



<a name="get-bed-by-id" />

* GET Bed By Id
  * URL ```http://BASE_URL/api/v1/bed/{id}```
  * Method ```GET```
```json
{
  "code": 200,
  "status": "Success",
  "data": {
    "id": 1,
    "kodeBed": "B1",
    "noBed": "1",
    "kamar": {
      "id": 1,
      "kode": "KIRH-01",
      "namaKamar": "Kamar Irina H 1",
      "ruangan": {
        "id": 1,
        "ruangan": "Irina H Lantai 1",
        "unitLayanan": {
          "id": 2,
          "namaUnitLayanan": "Perawatan Penyakit Dalam",
          "status": "Y"
        },
        "status": "Y"
      },
      "kelas": {
        "id": 3,
        "kelas": "Kelas II",
        "tarif_inap": 75000,
        "idperbub": null,
        "status": "Y"
      },
      "status": "Y"
    },
    "status": "Terisi"
  }
}
```





