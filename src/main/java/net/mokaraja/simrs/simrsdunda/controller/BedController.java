package net.mokaraja.simrs.simrsdunda.controller;


import net.mokaraja.simrs.simrsdunda.entity.Bed;
import net.mokaraja.simrs.simrsdunda.model.WebResponse;
import net.mokaraja.simrs.simrsdunda.service.BedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
public class BedController {

    @Autowired
    private BedService bedService;

    @GetMapping("/bed")
    public WebResponse<List<Bed>> getAllBed() {
        List<Bed> data = bedService.getAllBed();
        return new WebResponse(HttpStatus.OK.value(), "Success", data);
    }

    @GetMapping("/bed/{id}")
    public WebResponse<Bed> getBedById(@PathVariable("id") Long id) {
        Optional<Bed> data = bedService.getBedById(id);
        return new WebResponse(HttpStatus.OK.value(), "Success", data);
    }
}
