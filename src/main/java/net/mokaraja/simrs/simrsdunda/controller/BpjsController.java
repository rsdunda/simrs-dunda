package net.mokaraja.simrs.simrsdunda.controller;


import net.mokaraja.simrs.simrsdunda.dto.kamar.ResponseDataKamar;
import net.mokaraja.simrs.simrsdunda.dto.ketersediaankamar.ResponseDataKetersediaanKamar;
import net.mokaraja.simrs.simrsdunda.dto.kunjungan.ResponseDataKunjungan;
import net.mokaraja.simrs.simrsdunda.dto.peserta.ResponseDataPeserta;
import net.mokaraja.simrs.simrsdunda.dto.poli.ResponseDataPoli;
import net.mokaraja.simrs.simrsdunda.service.BpjsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/")
public class BpjsController {

    @Autowired
    private BpjsService bpjsService;

    @GetMapping("bpjs/nokartu/{nokartu}")
    public ResponseDataPeserta getDataPeserta(@PathVariable("nokartu") String nokartu) {
        return bpjsService.getPesertaByNoKartu(nokartu);
    }


    @GetMapping("bpjs/kamar")
    public ResponseDataKamar getDataKamar() {
        return bpjsService.getListKamar();
    }

    @GetMapping("bpjs/kamartersedia")
    public ResponseDataKetersediaanKamar getKetersediaanKamar(@RequestParam("kodeppk") String kodeppk, @RequestParam("start") int start, @RequestParam("limit") int limit) {
        return bpjsService.getKamarTersedia(kodeppk, start, limit);
    }

    @GetMapping("bpjs/poli")
    public ResponseDataPoli getDataPoli(@RequestParam("kode") String kode) {
        return bpjsService.getDataPoli(kode);
    }

    @GetMapping("bpjs/kunjungan")
    public ResponseDataKunjungan getDataKunjungan(@RequestParam("tanggal") String tanggal, @RequestParam("jenis") String jenis) {
        return bpjsService.getDataKunjungan(tanggal, jenis);
    }

}
