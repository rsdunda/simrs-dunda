package net.mokaraja.simrs.simrsdunda.controller;


import net.mokaraja.simrs.simrsdunda.entity.Kamar;
import net.mokaraja.simrs.simrsdunda.model.WebResponse;
import net.mokaraja.simrs.simrsdunda.service.KamarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class KamarController {

    @Autowired
    private KamarService kamarService;


    @GetMapping("/kamar")
    public WebResponse<List<Kamar>> getAllKamar() {
        List<Kamar> data = kamarService.getAllKamar();
        return new WebResponse(HttpStatus.OK.value(), "Success", data);
    }

    @GetMapping("/kamar/{kode}")
    private WebResponse<Kamar> getKamarByKode(@PathVariable("kode") String kode){
        Kamar data = kamarService.getKamarByKode(kode);
        return new WebResponse(HttpStatus.OK.value(), "Success", data);
    }


}
