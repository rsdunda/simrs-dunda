package net.mokaraja.simrs.simrsdunda.controller;

import net.mokaraja.simrs.simrsdunda.entity.Pasien;
import net.mokaraja.simrs.simrsdunda.model.WebResponse;
import net.mokaraja.simrs.simrsdunda.service.PasienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class PasienController {

    @Autowired
    private PasienService pasienService;

    @GetMapping("/pasien")
    public WebResponse<List<Pasien>> getPasien() {
        List<Pasien> data = pasienService.getPasien();
        return new WebResponse(HttpStatus.OK.value(), "Success", data);
    }


    @GetMapping("/pasien/{nomr}")
    public WebResponse<Pasien> getPasienByNomr(@PathVariable("nomr") String nomr) {
        Pasien i = pasienService.getPasienByNomr(nomr);
        return new WebResponse(HttpStatus.OK.value(), "Success", i);
    }
}
