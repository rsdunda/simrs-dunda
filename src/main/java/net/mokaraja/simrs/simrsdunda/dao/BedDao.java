package net.mokaraja.simrs.simrsdunda.dao;


import net.mokaraja.simrs.simrsdunda.entity.Bed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BedDao extends JpaRepository<Bed, Long> {
}
