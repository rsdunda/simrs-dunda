package net.mokaraja.simrs.simrsdunda.dao;

import net.mokaraja.simrs.simrsdunda.entity.Kamar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KamarDao extends JpaRepository<Kamar, Long> {
    Kamar findByKode(String kode);
}
