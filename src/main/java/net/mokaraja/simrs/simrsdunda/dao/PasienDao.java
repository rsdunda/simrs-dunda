package net.mokaraja.simrs.simrsdunda.dao;

import net.mokaraja.simrs.simrsdunda.entity.Pasien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface PasienDao extends JpaRepository<Pasien, BigInteger> {
    Pasien findByNomr(String nomr);
}
