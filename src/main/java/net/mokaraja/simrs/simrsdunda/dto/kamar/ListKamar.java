package net.mokaraja.simrs.simrsdunda.dto.kamar;

import lombok.Data;

@Data
public class ListKamar {
    private String kodekelas;
    private String namakelas;
}
