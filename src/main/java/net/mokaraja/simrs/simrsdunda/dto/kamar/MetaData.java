package net.mokaraja.simrs.simrsdunda.dto.kamar;

import lombok.Data;

@Data
public class MetaData {
    private String code;
    private String message;
    private Long totalitems;
}
