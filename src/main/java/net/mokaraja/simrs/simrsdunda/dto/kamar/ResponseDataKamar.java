package net.mokaraja.simrs.simrsdunda.dto.kamar;

import lombok.Data;

@Data
public class ResponseDataKamar {
    private MetaData metadata;
    private ResponseData response;
}
