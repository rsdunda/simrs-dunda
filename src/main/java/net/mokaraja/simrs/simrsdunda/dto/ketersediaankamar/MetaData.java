package net.mokaraja.simrs.simrsdunda.dto.ketersediaankamar;

import lombok.Data;

@Data
public class MetaData {
    private Integer code;
    private String message;
    private Integer totalitems;
}

