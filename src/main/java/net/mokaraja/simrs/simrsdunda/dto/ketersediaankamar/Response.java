package net.mokaraja.simrs.simrsdunda.dto.ketersediaankamar;

import lombok.Data;

import java.util.List;

@Data
public class Response {
    private List<ListData> list;
}
