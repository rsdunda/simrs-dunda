package net.mokaraja.simrs.simrsdunda.dto.ketersediaankamar;

import lombok.Data;



@Data
public class ResponseDataKetersediaanKamar {
    private MetaData metadata;
    private Response response;
}



