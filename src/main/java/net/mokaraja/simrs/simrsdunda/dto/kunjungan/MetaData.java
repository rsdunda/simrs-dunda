package net.mokaraja.simrs.simrsdunda.dto.kunjungan;

import lombok.Data;

@Data
public class MetaData {
    private String code;
    private String message;
}
