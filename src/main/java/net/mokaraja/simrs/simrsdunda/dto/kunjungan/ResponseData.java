package net.mokaraja.simrs.simrsdunda.dto.kunjungan;

import lombok.Data;

import java.util.List;

@Data
public class ResponseData {
    private List<Sep> sep;
}
