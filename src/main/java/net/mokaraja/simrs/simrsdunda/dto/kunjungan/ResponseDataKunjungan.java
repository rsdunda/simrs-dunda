package net.mokaraja.simrs.simrsdunda.dto.kunjungan;

import lombok.Data;

@Data
public class ResponseDataKunjungan {
    private MetaData metaData;
    private ResponseData response;
}
