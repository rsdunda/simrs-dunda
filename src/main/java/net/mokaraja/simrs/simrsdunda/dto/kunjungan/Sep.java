package net.mokaraja.simrs.simrsdunda.dto.kunjungan;

import lombok.Data;

@Data
public class Sep {
    private String diagnosa;
    private String jnsPelayanan;
    private String kelasRawat;
    private String nama;
    private String noKartu;
    private String noSep;
    private String noRujukan;
    private String poli;
    private String tglPlgSep;
    private String tglSep;
}
