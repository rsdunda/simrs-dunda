package net.mokaraja.simrs.simrsdunda.dto.peserta;

import lombok.Data;

@Data
public class HakKelas {
    private String keterangan;
    private String kode;
}
