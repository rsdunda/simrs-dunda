package net.mokaraja.simrs.simrsdunda.dto.peserta;

import lombok.Data;

@Data
public class Informasi {
    private String dinsos;
    private String noSKTM;
    private String prolanisPRB;
}
