package net.mokaraja.simrs.simrsdunda.dto.peserta;

import lombok.Data;

@Data
public class JenisPeserta {
    private String keterangan;
    private String kode;
}
