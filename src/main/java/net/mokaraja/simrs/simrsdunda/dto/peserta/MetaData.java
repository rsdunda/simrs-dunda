package net.mokaraja.simrs.simrsdunda.dto.peserta;

import lombok.Data;

@Data
public class MetaData {
    private String code;
    private String message;
}
