package net.mokaraja.simrs.simrsdunda.dto.peserta;

import lombok.Data;

@Data
public class Mr {
    private String noMR;
    private String noTelepon;
}
