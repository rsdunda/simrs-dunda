package net.mokaraja.simrs.simrsdunda.dto.peserta;

import lombok.Data;

@Data
public class ProvUmum {
    private String kdProvider;
    private String nmProvider;
}
