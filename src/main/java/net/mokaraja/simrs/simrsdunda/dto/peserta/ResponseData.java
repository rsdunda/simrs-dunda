package net.mokaraja.simrs.simrsdunda.dto.peserta;

import lombok.Data;

@Data
public class ResponseData {
    private Peserta peserta;
}
