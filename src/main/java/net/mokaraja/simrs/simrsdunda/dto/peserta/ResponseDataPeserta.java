package net.mokaraja.simrs.simrsdunda.dto.peserta;

import lombok.Data;

@Data
public class ResponseDataPeserta {
    private MetaData metaData;
    private ResponseData response;
}
