package net.mokaraja.simrs.simrsdunda.dto.peserta;

import lombok.Data;

@Data
public class Umur {
    private String umurSaatPelayanan;
    private String umurSekarang;
}
