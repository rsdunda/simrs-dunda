package net.mokaraja.simrs.simrsdunda.dto.poli;

import lombok.Data;

@Data
public class Poli {
    private String kode;
    private String nama;
}
