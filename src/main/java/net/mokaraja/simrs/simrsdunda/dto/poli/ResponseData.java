package net.mokaraja.simrs.simrsdunda.dto.poli;

import lombok.Data;

import java.util.List;

@Data
public class ResponseData {
    private List<Poli> poli;
}
