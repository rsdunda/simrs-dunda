package net.mokaraja.simrs.simrsdunda.dto.poli;

import lombok.Data;

@Data
public class ResponseDataPoli {
    private MetaData metaData;
    private ResponseData response;
}
