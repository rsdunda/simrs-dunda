package net.mokaraja.simrs.simrsdunda.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "m_bed")
public class Bed {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(name = "ID")
    private Long id;

    @Column(name = "KODE_BED")
    private String kodeBed;

    @Column(name = "NO_BED")
    private String noBed;

    @ManyToOne() @JoinColumn(name = "IDKAMAR")
    private Kamar kamar;

    @Column(name = "STATUS")
    private String status;
}
