package net.mokaraja.simrs.simrsdunda.entity;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "m_kamar")
public class Kamar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(name = "ID")
    private Long id;

    @Column(name = "KODE")
    private String kode;

    @Column(name = "NAMA_KAMAR")
    private String namaKamar;

    @ManyToOne() @JoinColumn(name = "ID_RUANGAN")
    private Ruangan ruangan;

    @ManyToOne() @JoinColumn(name = "IDKELAS")
    private Kelas kelas;

    @Column(name = "STATUS")
    private String status;

}
