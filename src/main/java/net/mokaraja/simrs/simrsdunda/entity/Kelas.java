package net.mokaraja.simrs.simrsdunda.entity;


import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "m_kelas")
public class Kelas {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(name = "ID")
    private Long id;

    @Column(name = "KELAS")
    private String kelas;

    @Column(name = "TARIF_INAP")
    private BigDecimal tarif_inap;


    @Column(name = "IDPERBUB")
    private String idperbub;


    @Column(name = "STATUS")
    private String status;
}
