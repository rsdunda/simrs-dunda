package net.mokaraja.simrs.simrsdunda.entity;


import lombok.Data;

import javax.persistence.*;
import java.math.BigInteger;

@Data
@Entity
@Table(name = "m_pasien")
public class Pasien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @Column(name = "NOMR")
    private String nomr;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "NAMA")
    private String nama;

    @Column(name = "TEMPAT")
    private String tempat;

    @Column(name = "tgllahir")
    private String tgllahir;

    @Column(name = "JENISKELAMIN")
    private String jenisKelamin;

    @Column(name = "ALAMAT")
    private String alamat;

    @Column(name = "KELURAHAN")
    private String kelurahan;

    @Column(name = "KDKECAMATAN")
    private String kdKecamatan;

    @Column(name = "KOTA")
    private String kota;

    @Column(name = "KDPROVINSI")
    private String kdProvinsi;

    @Column(name = "NOTELP")
    private String noTelp;

    @Column(name = "SUAMI_ORTU")
    private String suamiOrtu;

    @Column(name = "PEKERJAAN")
    private String pekerjaan;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "AGAMA")
    private String agama;

    @Column(name = "PENDIDIKAN")
    private String pendidikan;

    @Column(name = "KDCARABAYAR")
    private String kdCaraBayar;

    @Column(name = "NIP")
    private String nip;

    @Column(name = "TGLDAFTAR")
    private String tglDaftar;

    @Column(name = "ALAMAT_KTP")
    private String alamatKtp;

    @Column(name = "PARENT_NOMR")
    private String parentNomr;

    @Column(name = "PENANGGUNGJAWAB_NAMA")
    private String penanggungJawabNama;

    @Column(name = "PENANGGUNGJAWAB_HUBUNGAN")
    private String penanggungJawabHubungan;

    @Column(name = "PENANGGUNGJAWAB_ALAMAT")
    private String penanggungJawabAlamat;

    @Column(name = "PENANGGUNGJAWAB_PHONE")
    private String penanggungJawabPhone;

    @Column(name = "NOMR_LAMA")
    private String nomrLama;

    @Column(name = "NO_KARTU")
    private String noKartu;

    @Column(name = "JNS_PASIEN")
    private String jnsKartu;

    @Column(name = "KDPROVIDER")
    private String kdProvider;

    @Column(name = "NMPROVIDER")
    private String nmProvider;

    @Column(name = "Kelas")
    private String kelas;

    @Column(name = "DUKCAPIL")
    private String dukcapil;

    @Column(name = "KEL")
    private String kel;

    @Column(name = "KEC")
    private String kec;

    @Column(name = "KAB")
    private String kab;

    @Column(name = "PROV")
    private String prov;

    @Column(name = "GOL_DARAH")
    private String golDarah;
}
