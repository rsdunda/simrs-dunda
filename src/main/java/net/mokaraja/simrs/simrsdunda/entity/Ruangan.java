package net.mokaraja.simrs.simrsdunda.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "m_ruangan")
public class Ruangan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(name = "ID")
    private Long id;

    @Column(name = "RUANGAN")
    private String ruangan;

    @ManyToOne() @JoinColumn(name = "IDUNITLAYANAN")
    private UnitLayanan unitLayanan;

    @Column(name = "STATUS")
    private String status;
}
