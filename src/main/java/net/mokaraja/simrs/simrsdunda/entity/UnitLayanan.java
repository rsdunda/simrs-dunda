package net.mokaraja.simrs.simrsdunda.entity;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "m_unit_layanan")
public class UnitLayanan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(name = "ID")
    private Long id;

    @Column(name = "NAMA_UNIT_LAYANAN")
    private String namaUnitLayanan;



    @Column(name = "STATUS")
    private String status;
}
