package net.mokaraja.simrs.simrsdunda.service;

import net.mokaraja.simrs.simrsdunda.dao.BedDao;
import net.mokaraja.simrs.simrsdunda.entity.Bed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BedService {

    @Autowired
    private BedDao bedDao;

    public List<Bed> getAllBed() {
        return bedDao.findAll();
    }

    public Optional<Bed> getBedById(Long id) {
        return bedDao.findById(id);
    }
}
