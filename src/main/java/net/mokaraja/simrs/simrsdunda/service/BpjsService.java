package net.mokaraja.simrs.simrsdunda.service;

import net.mokaraja.simrs.simrsdunda.dto.kamar.ResponseDataKamar;
import net.mokaraja.simrs.simrsdunda.dto.ketersediaankamar.ResponseDataKetersediaanKamar;
import net.mokaraja.simrs.simrsdunda.dto.kunjungan.ResponseDataKunjungan;
import net.mokaraja.simrs.simrsdunda.dto.peserta.ResponseDataPeserta;
import net.mokaraja.simrs.simrsdunda.dto.poli.ResponseDataPoli;
import net.mokaraja.simrs.simrsdunda.utils.BpjsApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

@Service
public class BpjsService extends BpjsApi {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private BpjsApi bpjsApi;

    public ResponseDataPeserta getPesertaByNoKartu(String nokartu) {
        LocalDateTime date = LocalDateTime.now();
        return bpjsApi.dataPeserta("Peserta/nokartu/", nokartu + "/tglSEP/" + date, HttpMethod.GET, restTemplate);
    }

    public ResponseDataKamar getListKamar() {
        return bpjsApi.dataKamar("aplicaresws/rest/ref/kelas" , HttpMethod.GET, restTemplate);
    }

    public ResponseDataKetersediaanKamar getKamarTersedia(String kodeppk, int start, int limit) {
        return bpjsApi.dataKetersediaanKamar("aplicaresws/rest/bed/read/", start, limit, HttpMethod.GET, restTemplate);
    }

    public ResponseDataPoli getDataPoli(String kode) {
        return bpjsApi.dataPoli("referensi/poli/", kode, HttpMethod.GET, restTemplate);
    }

    public ResponseDataKunjungan getDataKunjungan(String tanggal, String jenis) {
        return bpjsApi.dataKunjungan("Monitoring/Kunjungan/Tanggal/", tanggal, jenis, HttpMethod.GET, restTemplate);
    }
}
