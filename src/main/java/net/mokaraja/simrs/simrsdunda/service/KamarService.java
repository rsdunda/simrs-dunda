package net.mokaraja.simrs.simrsdunda.service;

import net.mokaraja.simrs.simrsdunda.dao.KamarDao;
import net.mokaraja.simrs.simrsdunda.entity.Kamar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KamarService {

    @Autowired
    private KamarDao kamarDao;

    public List<Kamar> getAllKamar() {
        return kamarDao.findAll();
    }

    public Kamar getKamarByKode(String kode) {
        return kamarDao.findByKode(kode);
    }
}
