package net.mokaraja.simrs.simrsdunda.service;

import net.mokaraja.simrs.simrsdunda.dao.PasienDao;
import net.mokaraja.simrs.simrsdunda.entity.Pasien;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PasienService {

    @Autowired
    private PasienDao pasienDao;

    public List<Pasien> getPasien() {
        return pasienDao.findAll();
    }


    public Pasien getPasienByNomr(String nomr) {
        return pasienDao.findByNomr(nomr);
    }

}
