package net.mokaraja.simrs.simrsdunda.bridging;

import com.thoughtworks.xstream.core.util.Base64Encoder;
import net.mokaraja.simrs.simrsdunda.dto.kamar.ResponseDataKamar;
import net.mokaraja.simrs.simrsdunda.dto.ketersediaankamar.ResponseDataKetersediaanKamar;
import net.mokaraja.simrs.simrsdunda.dto.kunjungan.ResponseDataKunjungan;
import net.mokaraja.simrs.simrsdunda.dto.peserta.ResponseDataPeserta;
import net.mokaraja.simrs.simrsdunda.dto.poli.ResponseDataPoli;
import net.mokaraja.simrs.simrsdunda.utils.BpjsApi;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.time.*;
import java.util.Calendar;


@SpringBootTest
public class BpjsApiTest {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private BpjsApi bpjsApi;



    @Test
    public void BpjsUtisTest() {
        ResponseDataKunjungan data = bpjsApi.dataKunjungan("Monitoring/Kunjungan/Tanggal", "2017-10-01", "R.Inap", HttpMethod.GET, restTemplate);
        System.out.println(data);
    }

    @Test
    public void BpjsUtisTest2() {
        LocalDateTime date = LocalDateTime.now();
        ResponseDataPeserta data = bpjsApi.dataPeserta("Peserta/nokartu/", "0001075619147/tglSEP/" + date.toLocalDate(), HttpMethod.GET, restTemplate);
        System.out.println(data);
    }


    @Test
    public void getKamarTest() {
        ResponseDataKamar data = bpjsApi.dataKamar("aplicaresws/rest/ref/kelas" , HttpMethod.GET, restTemplate);
        System.out.println(data);
    }


    @Test
    public void getKetersediaanKamarTest() {
        ResponseDataKetersediaanKamar data = bpjsApi.dataKetersediaanKamar("aplicaresws/rest/bed/read/", 1, 1, HttpMethod.GET, restTemplate);
        System.out.println(data);
    }

    @Test
    public void getDataPoliTest() {
        ResponseDataPoli data = bpjsApi.dataPoli("referensi/poli/", "ICU", HttpMethod.GET, restTemplate);
        System.out.println(data);
    }


}
